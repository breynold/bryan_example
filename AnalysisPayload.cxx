// Add a submodule for JetSelectionHelper
#include "JetSelectionHelper/JetSelectionHelper.h"
// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

int main() {

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  event.readFrom( iFile.get() );

  // histograms for storage
  TH1D *h_njets_raw = new TH1D("h_njets_raw","",20,0,20);
  TH1D *h_mjj_raw = new TH1D("h_mjj_raw","",100,0,500);
  TH1D *h_njets_kin = new TH1D("h_njets_kin","",20,0,20);
  TH1D *h_mjj_kin = new TH1D("h_mjj_kin","",100,0,500);
  TH1D *h_njets_b = new TH1D("h_njets_b","",20,0,20);
  TH1D *h_mjj_b = new TH1D("h_mjj_b","",100,0,500);
  
  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();

  // add jet selection helper
  JetSelectionHelper jet_selector;
  
  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    if (count % 1000 == 0) {
      std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;
    }

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    // make vector of jets
    std::vector<xAOD::Jet> jets_raw;
    std::vector<xAOD::Jet> jets_kin;
    std::vector<xAOD::Jet> jets_b;
    
    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // print the kinematics of each jet in the event
      //std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;

      //if (jet->pt() < 50e3) {
      //  continue;
      //}
      
      jets_raw.push_back(*jet);
      
      if (jet_selector.isJetGood(jet)){
	jets_kin.push_back(*jet);
      }
    }

    // fill histograms

    h_njets_raw->Fill(jets_raw.size());
      
    if(jets_raw.size() >= 2){
      h_mjj_raw->Fill( (jets_raw.at(0).p4()+jets_raw.at(1).p4()).M()/1000. );
    }

    // counter for the number of events analyzed thus far
    count += 1;
  }

  // open TFile, store hist outputs
  TFile *fout = new TFile("myOutputFile.root","RECREATE");
  h_njets_raw->Write();
  h_mjj_raw->Write();
  fout->Close();

  // exit from the main function cleanly
  return 0;
}
